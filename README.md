# Plugin CIOIDC

Pour s'authentifier avec OpenID Connect sous SPIP.


## Les objectifs de ce plugin

L'objectif est d'utiliser le login et le mot de passe stocké dans un serveur d'authentification OpenID Connect (OIDC) au lieu de ceux qui sont stockés dans SPIP. 

Cela évite à l'utilisateur de gérer ses mots de passe dans plusieurs sites (ou applications) et cela lui évite de s'authentifier à nouveau lorsqu'il passe d'un site à un autre.

Par ailleurs, OpenID Connect permet d'obtenir des informations sur l'utilisateur (par exemple : le prénom et le nom).


## Pourquoi OpenID Connect et pas OpenID ?

Les protocoles OpenID 1.0 et OpenID 2.0 ont été abandonnés ("deprecated") et il est proposé d'utiliser OpenID Connect (OIDC) à la place. 


## Documentation

[Documentation](https://contrib.spip.net/CIOIDC-OpenID-Connect)


## Compatibilité

Le plugin est compatible avec : 
- SPIP 3.2, 4.0, 4.1, 4.2.
- PHP 7.4, 8.0, 8.1, 8.2.


## Installation

Le plugin s'installe comme tous les plugins.

Après l'installation, il convient de recalculer la page qui contient le formulaire de login de SPIP.

Le plugin contient la librairie jumbojett/OpenID-Connect-PHP (ce qui évite de devoir installer cette dernière).

Cette librairie nécessite les extensions de PHP CURL et JSON.

Remarque : 
Si le serveur d'authentification OpenID Connect n'accepte pas le point d'interrogation dans l'URL de redirection (https://adresse du site web/spip.php?action=login_cioidc) :
- dans le cas où on peut ajouter une règle de réécriture d'URL, on peut utiliser l'URL de redirection https://adresse du site web/cioidc.php et ajouter la règle de réécriture suivante :
RewriteRule ^cioidc.php$ spip.php?action=login_cioidc [QSA,L]
- dans le cas contraire, on peut utiliser l'URL de redirection https://adresse du site web/cioidc.php et recopier, à la racine du site, le fichier cioidc.php qui est situé à la racine du plugin. Si le plugin cioidc est dans le dossier plugins-dist (et pas dans le dossier plugins), il convient de remplacer /plugins/ par /plugins-dist/ dans la copie du fichier cioidc.php.


## Configuration

[Configuration](https://contrib.spip.net/CIOIDC)


## Paramétrage par constantes

Un paramétrage par constantes est possible. Il est prioritaire sur le paramétrage du plugin dans l'espace privé.

Pour en savoir plus, il convient de consulter le fichier suivant, qui figure à la racine du plugin : cioidc/_config_cioidc.txt

Il décrit comment procéder et contient des explications pour chaque constante.


## Pipelines offerts

Pipeline 'cioidc_userinfo' :
- objectif : effectuer d'autres actions avec les informations sur l'utilisateur.
- paramètre args : (tableau associatif) Identifiant de l'utilisateur dans le serveur d'authentification (OIDC) => valeur de cet identifiant
- paramètre data : (objet) userInfo (au sens OpenID Connect)
- retour attendu : aucun

Pipeline 'cioidc_auteur' :
- objectif : effectuer d'autres actions lorsque l'authentification sur le serveur OIDC a réussi mais que l'auteur n'existait pas dans SPIP (et que la configuration ne prévoit pas la création automatique d'auteur).
- paramètre args : (tableau associatif) Identifiant de l'utilisateur dans le serveur d'authentification (OIDC) => valeur de cet identifiant
- paramètre data : (objet) userInfo (au sens OpenID Connect)
- retour attendu : (tableau associatif) auteur (c'est-à-dire un tableau équivalent au résultat d'une interrogation de la table 'spip_auteurs' pour cet auteur) 
