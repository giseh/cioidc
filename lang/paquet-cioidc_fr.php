<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [
	'cioidc_description' => 'Il convient de configurer le plugin avant utilisation. Il utilise la librairie jumbojett/openid-connect-php.',
	'cioidc_slogan' => 'Pour s\'authentifier avec OpenID Connect'
];
