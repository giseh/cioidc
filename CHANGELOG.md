# Changelog

Changelog de CIOIDC

## 1.5.0 - 2024-10-25

### Added

- Dans le cas de la création automatique d'auteur, si le userinfo contient 'name' (cela peut dépendre des scopes utilisés), utiliser son contenu comme 'nom' pour l'auteur dans SPIP.


## 1.4.0 - 2024-10-15

### Added

- La configuration du plugin CIOIDC dans l'espace privé est désormais réservée, par défaut, aux webmestres (au sens SPIP). Une constante permet de revenir au fonctionnement antérieur (cf. _config_cioidc.txt).
- Pour la création automatique d'auteurs, il est désormais possible de choisir de créer les auteurs avec le statut d'administrateur.


## 1.3.0 - 2024-09-23

### Added

- Il affiche automatiquement le bouton FranceConnect lorsque l'adresse du serveur d'authentification est une des adresses de FranceConnect.

### Changed

- Utilisation de la nouvelle version 1.0.2 de la librairie Jumbojett/OpenID-Connect-PHP (livrée avec le plugin).


## 1.2.0 - 2024-09-10

### Added

- Ajout d'une option de configuration supplémentaire : "le serveur OpenID Connect accepte-t-il le point d'interrogation dans la l'adresse de redirection ?"



## 1.1.0 - 2024-08-30

### Added

- En cas d'erreur rencontrée par le librairie jumbojett/OpenID-Connect-PHP (par exemple, lorsque l'URL du serveur OpenID Connect, renseignée dans la configuration du plugin, ne peut pas être atteinte par SPIP), CIOIDC affiche une page d'information pour l'utilisateur et écrit le message d'erreur dans les logs de SPIP.

